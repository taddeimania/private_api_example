from django.db import models

from rest_framework.authtoken.models import Token

from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class FavoriteCar(models.Model):
    user = models.ForeignKey('auth.User')
    color = models.CharField(max_length=10)
    year = models.IntegerField()
    make = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender="auth.User")
def create_token(**kwargs):
    created = kwargs.get("created")
    instance = kwargs.get("instance")
    if created:
        Token.objects.create(user=instance)












pass
