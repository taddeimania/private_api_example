from django.shortcuts import render
from rest_framework import generics

from app.serializers import FavoriteCarSerializer
from app.models import FavoriteCar


class FavoriteCarListAPIView(generics.ListAPIView):
    #queryset = FavoriteCar.objects.all()
    serializer_class = FavoriteCarSerializer

    def get_queryset(self):
        return FavoriteCar.objects.filter(user=self.request.user)
