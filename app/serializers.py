
from rest_framework import serializers

from app.models import FavoriteCar


class FavoriteCarSerializer(serializers.ModelSerializer):

    class Meta:
        model = FavoriteCar
