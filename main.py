import requests


username = input("Username? ")
password = input("Password? ")

post_data = {
    "username": username,
    "password": password
}

response = requests.post("http://localhost:8000/api-token-auth/", post_data).json()

token = response["token"]

headers = {
    "Authorization": "Token {}".format(token)
}

response = requests.get("http://localhost:8000/favorite_cars/", headers=headers).json()

for favorite_car in response:
    print(favorite_car["make"], favorite_car["year"])
